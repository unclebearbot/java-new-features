package org.bitbucket.unclebear.features.java11.feature1;

import java.util.Arrays;

public class OuterClassExample {
    public static void main(String[] args) {
        OuterClassExample outerClassExample = new OuterClassExample();
        outerClassExample.test();
    }

    private void test() {
        System.out.println(OuterClassExample.class.getNestHost());
        System.out.println(InnerClassExample.class.getNestHost());
        System.out.println(OuterClassExample.class.isNestmateOf(InnerClassExample.class));
        System.out.println(InnerClassExample.class.isNestmateOf(OuterClassExample.class));
        System.out.println(OuterClassExample.class.isNestmateOf(OuterClassExample.class));
        System.out.println(InnerClassExample.class.isNestmateOf(InnerClassExample.class));
        System.out.println(Arrays.toString(OuterClassExample.class.getNestMembers()));
        System.out.println(Arrays.toString(InnerClassExample.class.getNestMembers()));
    }

    private class InnerClassExample {
    }
}
