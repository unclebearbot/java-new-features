package org.bitbucket.unclebear.features.java11.feature1;

import java.lang.reflect.Method;

public class OuterClass {
    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        outerClass.test();
    }

    private void test() {
        InnerClass innerClass = new InnerClass();
        innerClass.innerMethod();
        innerClass.innerMethodReflection(this);
    }

    private void outerMethod() {
        System.out.println("Hello, world!");
    }

    private class InnerClass {
        private void innerMethod() {
            outerMethod();
        }

        private void innerMethodReflection(OuterClass outerClass) {
            try {
                Method method = outerClass.getClass().getDeclaredMethod("outerMethod");
                method.invoke(outerClass);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
