package org.bitbucket.unclebear.features.java12.feature1;

public class SwitchExample {
    public static void main(String[] args) {
        SwitchExample switchExample = new SwitchExample();
        switchExample.test();
    }

    private void test() {
        System.out.println(traditionalSwitch("SUNDAY"));
        System.out.println(traditionalSwitch("MONDAY"));
        System.out.println(enhancedSwitch("TUESDAY"));
        System.out.println(enhancedSwitch("WEDNESDAY"));
        System.out.println(functionalSwitch("THURSDAY"));
        System.out.println(functionalSwitch("FRIDAY"));
    }

    private int traditionalSwitch(String day) {
        switch (day) {
            case "MONDAY":
            case "TUESDAY":
            case "WEDNESDAY":
            case "THURSDAY":
            case "FRIDAY":
                return 1;
            case "SATURDAY":
            case "SUNDAY":
                return 0;
            default:
                throw new IllegalArgumentException(day);
        }
    }

    private int enhancedSwitch(String day) {
        switch (day) {
            case "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY" -> {
                return 1;
            }
            case "SATURDAY", "SUNDAY" -> {
                return 0;
            }
            default -> throw new IllegalArgumentException(day);
        }
    }

    private int functionalSwitch(String day) {
        return switch (day) {
            case "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY" -> 1;
            case "SATURDAY", "SUNDAY" -> 0;
            default -> throw new IllegalArgumentException(day);
        };
    }
}
