# Java New Features

## Java 9

###### September 2017

#### Feature 1 - Introduce Module System

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
$ cd java-9-feature-1
```

(1) Build the main application.

```text
$ gradle build -b app/build.gradle

BUILD SUCCESSFUL in 0s
4 actionable tasks: 4 executed
```

(2) Start the main application.

```text
$ java -p app/build/libs:plugin-api/build/libs -m org.bitbucket.unclebear.features.java9_.feature1_.app/org.bitbucket.unclebear.features.java9.feature1.app.VersionControlApplication

Starting Version Control Application
Loading all plugins
Stopping Version Control Application
Unloading all plugins
```

(3) Build plugins.

```text
$ gradle build -b git-plugin/build.gradle
$ gradle build -b mercury-plugin/build.gradle
$ gradle build -b subversion-plugin/build.gradle

BUILD SUCCESSFUL in 0s
4 actionable tasks: 4 executed
```

(4) Add plugins.

```text
$ cp *-plugin/build/libs/*.jar app/build/libs
```

(5) Start the main application again.

```text
$ java -p app/build/libs:plugin-api/build/libs -m org.bitbucket.unclebear.features.java9_.feature1_.app/org.bitbucket.unclebear.features.java9.feature1.app.VersionControlApplication

Starting Version Control Application
Loading all plugins
Loading Mercury plugin
Loading Git plugin
Loading Subversion plugin
Stopping Version Control Application
Unloading all plugins
Unloading Mercury plugin
Unloading Git plugin
Unloading Subversion plugin
```

#### Feature 2 - Introduce JMod/JImage Files

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
$ cd java-9-feature-2
```

(1) Make a simple module without any dependency.

```text
$ gradle build

BUILD SUCCESSFUL in 0s
2 actionable tasks: 2 executed
```

```text
$ ls build/libs

java-9-feature-2.jmod
```

(2) List the content.

```text
$ jmod list build/libs/java-9-feature-2.jmod

classes/module-info.class
classes/META-INF/MANIFEST.MF
classes/org/bitbucket/unclebear/features/java9/feature2/Main.class
```

(3) A module `requires java.base` implicitly, just like there is no need to `import java.lang.*` by ourselves.

```text
$ unzip -q build/libs/java-9-feature-2.jmod -d build/unzip
$ javap build/unzip/classes/module-info.class

Compiled from "module-info.java"
module org.bitbucket.unclebear.features.java9_.feature2_ {
  requires java.base;
}
```

(4) Create a customized image of JRE with essential modules only.

```text
$ jlink -p "${JAVA_HOME}/jmods:build/libs" --add-modules org.bitbucket.unclebear.features.java9_.feature2_ --compress 2 --launcher start=org.bitbucket.unclebear.features.java9_.feature2_/org.bitbucket.unclebear.features.java9.feature2.Main --output build/jre
```

```text
$ build/jre/bin/start

Hello, world!
```

```text
$ du -sh build/jre

24M     build/jre
```

```text
$ build/jre/bin/java --list-modules

java.base@9
org.bitbucket.unclebear.features.java9_.feature2_
```

#### Feature 3 - Introduce Multi-Release JAR Files

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
$ cd java-9-feature-3
```

```text
$ gradle build

BUILD SUCCESSFUL in 0s
3 actionable tasks: 3 executed
```

```text
$ /opt/java/8/bin/java -jar build/libs/java-9-feature-3.jar

Hello, world 8!
```

```text
$ /opt/java/9/bin/java -jar build/libs/java-9-feature-3.jar

Hello, world 9!
```

#### Feature 4 - Introduce JShell

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
```

```text
$ jshell

|  Welcome to JShell -- Version 9
|  For an introduction type: /help intro

jshell> String s = "Hello, world!"
s ==> "Hello, world!"

jshell> System.out.println(s)
Hello, world!

jshell> /exit
|  Goodbye
```

#### Feature 5 - Introduce Factory Methods for Immutable Collections

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
```

```text
$ jshell

|  Welcome to JShell -- Version 9
|  For an introduction type: /help intro

jshell> List l = List.of(1,2,3,4,5)
l ==> [1, 2, 3, 4, 5]

jshell> System.out.println(l)
[1, 2, 3, 4, 5]

jshell> System.out.println(l.getClass())
class java.util.ImmutableCollections$ListN

jshell> l.add(6)
|  java.lang.UnsupportedOperationException thrown:
|        at ImmutableCollections.uoe (ImmutableCollections.java:70)
|        at ImmutableCollections$AbstractImmutableList.add (ImmutableCollections.java:76)
|        at (#4:1)
```

#### Feature 6 - Introduce New HTTP Client and HTTP/2 Support *(Incubator)*

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
```

```text
$ jshell --add-module jdk.incubator.httpclient

|  Welcome to JShell -- Version 9
|  For an introduction type: /help intro

jshell> import jdk.incubator.http.*

jshell> HttpClient httpClient = HttpClient.newBuilder().followRedirects(HttpClient.Redirect.SECURE).version(HttpClient.Version.HTTP_2).build()
httpClient ==> jdk.incubator.http.HttpClientImpl@dbf57b3

jshell> HttpRequest httpRequest = HttpRequest.newBuilder().uri(new URI("http://www.google.com")).GET().build()
httpRequest ==> http://www.google.com GET

jshell> HttpResponse httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandler.asString())
httpResponse ==> jdk.incubator.http.HttpResponseImpl@6b419da

jshell> System.out.println(httpResponse.statusCode())
200

jshell> System.out.println(httpResponse.body().toString().split("\n")[0])
<!doctype html>
```

#### Feature 7 - Introduce Support for Container Memory Limitation *(Experimental)*

```text
$ export JAVA_HOME=/opt/java/9
$ export PATH=${JAVA_HOME}/bin:${PATH}
```

```text
$ java -XX:+UnlockExperimentalVMOptions -XX:+PrintFlagsFinal 2>/dev/null | grep UseCGroupMemoryLimitForHeap

     bool UseCGroupMemoryLimitForHeap              = false                               {experimental} {default}
```

```text
$ java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:+PrintFlagsFinal 2>/dev/null | grep UseCGroupMemoryLimitForHeap

     bool UseCGroupMemoryLimitForHeap              = true                                {experimental} {command line}
```

## Java 10

###### March 2018

#### Feature 1 - Introduce Local Variable Type Inference

Refer to [the example class](java-10-feature-1/src/main/java/org/bitbucket/unclebear/features/java10/feature1/TypeInference.java) for below points.

(1) Common usages.

(2) Lambda expression.

(3) Try with resources.

(4) Generic type.

(5) Polymorphic type.

#### Feature 2 - Mark the Flag of Container Memory Limitation Support as Deprecated

```text
$ export JAVA_HOME=/opt/java/10
$ export PATH=${JAVA_HOME}/bin:${PATH}
```

```text
$ java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:+PrintFlagsFinal 2>&1 | grep deprecated

OpenJDK 64-Bit Server VM warning: Option UseCGroupMemoryLimitForHeap was deprecated in version 10.0 and will likely be removed in a future release.
```

## Java 11 (LTS)

###### September 2018

#### Feature 1 - Nest-Based Access Control

```text
$ cd java-11-feature-1
```

(1) Build and run the example class with Java 11.

```text
$ mkdir -p build/classes
$ /opt/java/11/bin/javac -g -cp src/main/java -d build/classes src/main/java/org/bitbucket/unclebear/features/java11/feature1/OuterClass.java
$ /opt/java/11/bin/java -cp build/classes org.bitbucket.unclebear.features.java11.feature1.OuterClass

Hello, world!
Hello, world!
```

(2) Build and run it again with Java 8.

```text
$ mkdir -p build/classes8
$ /opt/java/8/bin/javac -g -cp src/main/java -d build/classes8 src/main/java/org/bitbucket/unclebear/features/java11/feature1/OuterClass.java
$ /opt/java/8/bin/java -cp build/classes8 org.bitbucket.unclebear.features.java11.feature1.OuterClass

Hello, world!
java.lang.IllegalAccessException: Class org.bitbucket.unclebear.features.java11.feature1.OuterClass$InnerClass can not access a member of class org.bitbucket.unclebear.features.java11.feature1.OuterClass with modifiers "private"
        at sun.reflect.Reflection.ensureMemberAccess(Reflection.java:102)
        at java.lang.reflect.AccessibleObject.slowCheckMemberAccess(AccessibleObject.java:296)
        at java.lang.reflect.AccessibleObject.checkAccess(AccessibleObject.java:288)
        at java.lang.reflect.Method.invoke(Method.java:490)
        at org.bitbucket.unclebear.features.java11.feature1.OuterClass$InnerClass.innerMethodReflection(OuterClass.java:29)
        at org.bitbucket.unclebear.features.java11.feature1.OuterClass$InnerClass.access$200(OuterClass.java:21)
        at org.bitbucket.unclebear.features.java11.feature1.OuterClass.test(OuterClass.java:14)
        at org.bitbucket.unclebear.features.java11.feature1.OuterClass.main(OuterClass.java:8)
```

(3) Decompile the classes generated by Java 11. Only key lines are shown below.

```text
$ /opt/java/11/bin/javap -c -p build/classes/org/bitbucket/unclebear/features/java11/feature1/OuterClass.class

Compiled from "OuterClass.java"
public class org.bitbucket.unclebear.features.java11.feature1.OuterClass {
  public org.bitbucket.unclebear.features.java11.feature1.OuterClass();

  private void outerMethod();
    Code:
       0: getstatic     #9                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: ldc           #10                 // String Hello, world!
       5: invokevirtual #11                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
       8: return
}
```

```text
$ /opt/java/11/bin/javap -c -p build/classes/org/bitbucket/unclebear/features/java11/feature1/OuterClass\$InnerClass.class

Compiled from "OuterClass.java"
class org.bitbucket.unclebear.features.java11.feature1.OuterClass$InnerClass {
  final org.bitbucket.unclebear.features.java11.feature1.OuterClass this$0;

  private void innerMethod();
    Code:
       0: aload_0
       1: getfield      #1                  // Field this$0:Lorg/bitbucket/unclebear/features/java11/feature1/OuterClass;
       4: invokevirtual #3                  // Method org/bitbucket/unclebear/features/java11/feature1/OuterClass.outerMethod:()V
       7: return
}
```

(4) Decompile the classes generated by Java 8. Only key lines are shown below.

```text
$ /opt/java/8/bin/javap -c -p build/classes8/org/bitbucket/unclebear/features/java11/feature1/OuterClass.class

Compiled from "OuterClass.java"
public class org.bitbucket.unclebear.features.java11.feature1.OuterClass {
  public org.bitbucket.unclebear.features.java11.feature1.OuterClass();

  private void outerMethod();
    Code:
       0: getstatic     #10                 // Field java/lang/System.out:Ljava/io/PrintStream;
       3: ldc           #11                 // String Hello, world!
       5: invokevirtual #12                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
       8: return

  static void access$300(org.bitbucket.unclebear.features.java11.feature1.OuterClass);
    Code:
       0: aload_0
       1: invokespecial #1                  // Method outerMethod:()V
       4: return
}
```

```text
$ /opt/java/8/bin/javap -c -p build/classes8/org/bitbucket/unclebear/features/java11/feature1/OuterClass\$InnerClass.class

Compiled from "OuterClass.java"
class org.bitbucket.unclebear.features.java11.feature1.OuterClass$InnerClass {
  final org.bitbucket.unclebear.features.java11.feature1.OuterClass this$0;

  private void innerMethod();
    Code:
       0: aload_0
       1: getfield      #4                  // Field this$0:Lorg/bitbucket/unclebear/features/java11/feature1/OuterClass;
       4: invokestatic  #6                  // Method org/bitbucket/unclebear/features/java11/feature1/OuterClass.access$300:(Lorg/bitbucket/unclebear/features/java11/feature1/OuterClass;)V
       7: return
}
```

(5) Explanations for above behaviors.

* The root cause is that in Java 8, it is not permitted by JVM, to access private methods across classes, even if they are nested - like `OuterClass` and `InnerClass` above.

* However, the compiler (javac) permits the access by introducing bridge method - like `OuterClass.access$300()` above.

* In Java 11, the concept of nest host/mate with related access rules is added to JVM, nest mates in same nest host can access each other's private methods. By this, the bridge method is not required anymore.

* To achieve this, the class file format has been changed to include two new attributes:

    * One nest member, typically the top-level class - like `OuterClass` above, is regarded as the nest host. It contains an attribute `NestMembers` to identify the other nest members.

    * Each of the other nest members  - like `InnerClass` above, has an attribute `NestHost` to identify its nest host.

(6) New methods in `java.lang.Class` for nest information.

```text
$ /opt/java/11/bin/javac -cp src/main/java -d build/classes src/main/java/org/bitbucket/unclebear/features/java11/feature1/OuterClassExample.java
$ /opt/java/11/bin/java -cp build/classes org.bitbucket.unclebear.features.java11.feature1.OuterClassExample

class org.bitbucket.unclebear.features.java11.feature1.OuterClassExample
class org.bitbucket.unclebear.features.java11.feature1.OuterClassExample
true
true
true
true
[class org.bitbucket.unclebear.features.java11.feature1.OuterClassExample, class org.bitbucket.unclebear.features.java11.feature1.OuterClassExample$InnerClassExample]
[class org.bitbucket.unclebear.features.java11.feature1.OuterClassExample, class org.bitbucket.unclebear.features.java11.feature1.OuterClassExample$InnerClassExample]
```

#### Feature 2 - Launch Single Source Code File Directly

```text
$ export JAVA_HOME=/opt/java/11
$ export PATH=${JAVA_HOME}/bin:${PATH}
$ cd java-11-feature-2
```

(1) Common usage.

```text
$ cd src/main/java/org/bitbucket/unclebear/features/java11/feature2
$ java SingleFileExample.java Lucas

Hello, Lucas!
```

(2) More Linux style (just for fun).

```text
$ sed '1 i #!java --source 11' SingleFileExample.java > greet
$ chmod +x greet
$ ./greet Lucas

Hello, Lucas!
```

```text
$ rm greet
```

#### Feature 3 - Epsilon GC

```text
$ export JAVA_HOME=/opt/java/11
$ export PATH=${JAVA_HOME}/bin:${PATH}
$ cd java-11-feature-3
```

(1) Build the example application.

```text
$ gradle build

BUILD SUCCESSFUL in 0s
2 actionable tasks: 2 executed
```

(2) Run the application with 128 MB of memory.

```text
$ java -Xmx128m -jar build/libs/java-11-feature-3.jar

Garbage: 0
GC: 1
GC: 474368
Garbage: 2097152
```

(3) Run it again using Epsilon GC.

```text
$ java -Xmx128m -XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC -jar build/libs/java-11-feature-3.jar

Garbage: 0
GC: 0
Garbage: 2097152
```

(4) Run the application with 96 MB of memory.

```text
$ java -Xmx96m -jar build/libs/java-11-feature-3.jar

Garbage: 0
GC: 1
GC: 551417
Garbage: 2097152
```

(5) Run it again using Epsilon GC.

```text
$ java -Xmx96m -XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC -jar build/libs/java-11-feature-3.jar

Garbage: 0
Terminating due to java.lang.OutOfMemoryError: Java heap space
```

(6) Simple benchmark.

```text
$ time for i in {1..100}; do java -Xmx128m -jar build/libs/java-11-feature-3.jar > /dev/null; done

real    1m53.489s
user    0m0.166s
sys     0m0.392s
```

```text
$ time for i in {1..100}; do java -Xmx128m -XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC -jar build/libs/java-11-feature-3.jar > /dev/null; done

real    0m39.069s
user    0m0.196s
sys     0m0.366s
```

(7) Primary scenarios.

* Applications with short lifecycle, for batter responsiveness and larger throughput.

* Accurate performance testing without interference of GC.

* Fast-fail smoke testing when the application takes more memory than expected.

#### Feature 4 - Standardize the Incubated HTTP Client introduced in Java 9

```text
$ /opt/java/11/bin/jshell

|  Welcome to JShell -- Version 11
|  For an introduction type: /help intro

jshell> import java.net.http.*

jshell> HttpRequest httpRequest = HttpRequest.newBuilder().uri(URI.create("http://www.google.com")).build()
httpRequest ==> http://www.google.com GET

jshell> HttpResponse httpResponse = HttpClient.newHttpClient().send(httpRequest, HttpResponse.BodyHandlers.ofString())
httpResponse ==> (GET http://www.google.com) 200

jshell> System.out.println(httpResponse.statusCode())
200

jshell> System.out.println(httpResponse.body().toString().split("\n")[0])
<!doctype html>
```

#### Feature 5 - Make Container Memory Limitation Support Enabled by Default

```text
$ /opt/java/11/bin/java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:+PrintFlagsFinal

Unrecognized VM option 'UseCGroupMemoryLimitForHeap'
Error: Could not create the Java Virtual Machine.
Error: A fatal exception has occurred. Program will exit.
```

## Java 12

###### March 2019

#### Feature 1 - Enhanced Switch Expression *(Preview)*

```text
$ export JAVA_HOME=/opt/java/12
$ export PATH=${JAVA_HOME}/bin:${PATH}
$ cd java-12-feature-1
```

Refer to [the example class](java-12-feature-1/src/main/java/org/bitbucket/unclebear/features/java12/feature1/SwitchExample.java) and run it.

```text
$ gradle build

BUILD SUCCESSFUL in 0s
2 actionable tasks: 2 executed
```

```text
$ java --enable-preview -jar build/libs/java-12-feature-1.jar

0
1
1
1
1
1
```

#### Feature 2 - Some New Methods

(1) To transform strings

```text
$ /opt/java/12/bin/jshell

|  Welcome to JShell -- Version 12
|  For an introduction type: /help intro

jshell> var s = "upper".transform(it -> it + "case").transform(String::toUpperCase)
s ==> "UPPERCASE"
```

(2) To format numbers

```text
$ /opt/java/12/bin/jshell

|  Welcome to JShell -- Version 12
|  For an introduction type: /help intro

jshell> import java.text.NumberFormat

jshell> var f = NumberFormat.getCompactNumberInstance(Locale.US, NumberFormat.Style.LONG)
f ==> java.text.CompactNumberFormat@bf4983f7

jshell> var s = f.format(10)
s ==> "10"

jshell> var s = f.format(10_000)
s ==> "10 thousand"

jshell> var s = f.format(10_000_000)
s ==> "10 million"

jshell> var s = f.format(10_000_000_000L)
s ==> "10 billion"

jshell> var s = f.format(10_000_000_000_000L)
s ==> "10 trillion"

jshell> var s = f.format(10_000_000_000_000_000L)
s ==> "10000 trillion"

jshell> var s = f.format(10_000_000_000_000_000_000D)
s ==> "10000000 trillion"
```

## Java 13

###### September 2019

#### Feature 1 - Enhanced Again Switch Expression *(Preview)*

```text
$ /opt/java/13/bin/jshell --enable-preview

|  Welcome to JShell -- Version 13
|  For an introduction type: /help intro

jshell> int type(String day) {
   ...>     return switch ("FRIDAY") {
   ...>         case "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY" -> {
   ...>             yield 1;
   ...>         }
   ...>         case "SATURDAY", "SUNDAY" -> {
   ...>             yield 0;
   ...>         }
   ...>         default -> throw new IllegalArgumentException(day);
   ...>     };
   ...> }
|  created method type(String)

jshell> var type = type("FRIDAY")
type ==> 1
```

#### Feature 2 - Text Blocks *(Preview)*

```text
$ /opt/java/13/bin/jshell --enable-preview

|  Welcome to JShell -- Version 13
|  For an introduction type: /help intro

jshell> var tpl = """
   ...> <html>
   ...> <body>
   ...> <p>%s</p>
   ...> </body>
   ...> </html>
   ...> """
tpl ==> "<html>\n<body>\n<p>%s</p>\n</body>\n</html>\n"

jshell> var html = String.format(tpl, "Hello, world!")
html ==> "<html>\n<body>\n<p>Hello, world!</p>\n</body>\n</html>\n"
```

## Java 14

###### March 2020

## Java 15

###### September 2020

## Java 16

###### March 2021

## Java 17 (LTS)

###### September 2021
