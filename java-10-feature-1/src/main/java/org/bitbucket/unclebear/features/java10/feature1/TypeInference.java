package org.bitbucket.unclebear.features.java10.feature1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TypeInference {
    public static void main(String[] args) {
        TypeInference typeInference = new TypeInference();
        typeInference.commonUsages();
        typeInference.lambdaExpression();
        typeInference.tryWithResources();
        typeInference.genericType();
        typeInference.polymorphicType();
    }

    private void commonUsages() {
        var greet = "Hello, world!";
        System.out.println(greet); // "Hello, world!"
        /*
            Variables still have static types, below code cannot be compiled.
            greet = 1;

            Furthermore, it's not valid to declare a variable without initializer.
            var a;
            a = 1;
         */
    }

    private void lambdaExpression() {
        var users = Map.of("Lisa", 19, "Alice", 15, "Jessica", 17);
        Predicate<Map.Entry<String, Integer>> isChild = entry -> entry.getValue() < 18;
        var children = users.entrySet().stream().filter(isChild).map(Map.Entry::getKey).collect(Collectors.toList());
        System.out.println(children); // [Jessica, Alice]
        /*
            Type inference is not available in Lambda expression, below code cannot be compiled.
            var isChild = (Map.Entry<String, Integer> entry) -> entry.getValue() < 18;
         */
    }

    private void tryWithResources() {
        var path = Paths.get("readme.md");
        try (var lines = Files.lines(path)) {
            lines.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void genericType() {
        List<String> list1 = new ArrayList<>();
        list1.add("test");
        String s1 = list1.get(0);
        List list2 = new ArrayList<>();
        list2.add("test");
        Object o2 = list2.get(0);
        var list3 = new ArrayList<String>();
        list3.add("test");
        String s3 = list3.get(0);
        var list4 = new ArrayList<>();
        list4.add("test");
        Object o4 = list4.get(0);
        System.out.println(list1.getClass()); // class java.util.ArrayList
        System.out.println(list2.getClass()); // class java.util.ArrayList
        System.out.println(list3.getClass()); // class java.util.ArrayList
        System.out.println(list4.getClass()); // class java.util.ArrayList
    }

    private void polymorphicType() {
        Map<String, Integer> map1 = new HashMap<>();
        System.out.println(map1.getClass()); // class java.util.HashMap
        var map2 = new HashMap<String, Integer>();
        System.out.println(map2.getClass()); // class java.util.HashMap
        map1 = Map.copyOf(map2);
        /*
            The type of map2 is HashMap (implementation) instead of Map (interface), below code cannot be compiled.
            map2 = Map.copyOf(map1);
         */
    }
}
