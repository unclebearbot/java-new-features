package org.bitbucket.unclebear.features.java11.feature3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class EpsilonExample {
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
    private static List<Garbage> recycleBin = new ArrayList<>();
    private static int garbageCount = 0;
    private static int gcCount = 0;
    private static Lock gcLock = new ReentrantLock();

    public static void main(String[] args) {
        EpsilonExample epsilonExample = new EpsilonExample();
        epsilonExample.test();
    }

    private void test() {
        countGarbage();
        Runtime.getRuntime().addShutdownHook(new Thread(this::countGarbage));
        Collections.nCopies(1024 * 1024 * 2, null).forEach(this::manufactureGarbage);
        countGc();
    }

    private void countGarbage() {
        System.out.println("Garbage: " + garbageCount);
    }

    private void manufactureGarbage(Object object) {
        ++garbageCount;
        recycleBin.add(new Garbage());
        recycleBin.clear();
    }

    private static void countGc() {
        System.out.println("GC: " + gcCount);
    }

    private static class Garbage {
        @SuppressWarnings("deprecation")
        @Override
        public void finalize() {
            gcLock.lock();
            increaseGcCount();
            gcLock.unlock();
        }

        private void increaseGcCount() {
            ++gcCount;
            if (gcCount == 1) {
                countGc();
            }
        }
    }
}
