package org.bitbucket.unclebear.features.java9.feature1.plugin.git;

import org.bitbucket.unclebear.features.java9.feature1.plugin.api.Plugin;

public class GitPlugin implements Plugin {
    @Override
    public void load() {
        System.out.println("Loading Git plugin");
    }

    @Override
    public void unload() {
        System.out.println("Unloading Git plugin");
    }
}
