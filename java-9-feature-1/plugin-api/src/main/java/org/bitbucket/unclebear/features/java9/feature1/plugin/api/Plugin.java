package org.bitbucket.unclebear.features.java9.feature1.plugin.api;

public interface Plugin {
    void load();

    void unload();
}
