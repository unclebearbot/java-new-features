module org.bitbucket.unclebear.features.java9_.feature1_.plugin.subversion {
    requires org.bitbucket.unclebear.features.java9_.feature1_.plugin.api;
    provides org.bitbucket.unclebear.features.java9.feature1.plugin.api.Plugin with org.bitbucket.unclebear.features.java9.feature1.plugin.subversion.SubversionPlugin;
}
