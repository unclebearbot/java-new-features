package org.bitbucket.unclebear.features.java9.feature1.plugin.subversion;

import org.bitbucket.unclebear.features.java9.feature1.plugin.api.Plugin;

public class SubversionPlugin implements Plugin {
    @Override
    public void load() {
        System.out.println("Loading Subversion plugin");
    }

    @Override
    public void unload() {
        System.out.println("Unloading Subversion plugin");
    }
}
