package org.bitbucket.unclebear.features.java9.feature1.plugin.mercury;

import org.bitbucket.unclebear.features.java9.feature1.plugin.api.Plugin;

public class MercuryPlugin implements Plugin {
    @Override
    public void load() {
        System.out.println("Loading Mercury plugin");
    }

    @Override
    public void unload() {
        System.out.println("Unloading Mercury plugin");
    }
}
