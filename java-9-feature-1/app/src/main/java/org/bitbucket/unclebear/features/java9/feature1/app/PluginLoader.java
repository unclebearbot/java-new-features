package org.bitbucket.unclebear.features.java9.feature1.app;

import org.bitbucket.unclebear.features.java9.feature1.plugin.api.Plugin;

import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

class PluginLoader {
    private List<Plugin> plugins;

    PluginLoader() {
        ServiceLoader<Plugin> serviceLoader = ServiceLoader.load(Plugin.class);
        plugins = serviceLoader.stream().map(ServiceLoader.Provider::get).collect(Collectors.toList());
    }

    void loadAll() {
        System.out.println("Loading all plugins");
        plugins.forEach(Plugin::load);
    }

    void unloadAll() {
        System.out.println("Unloading all plugins");
        plugins.forEach(Plugin::unload);
    }
}
