package org.bitbucket.unclebear.features.java9.feature1.app;

public class VersionControlApplication {
    private static VersionControlApplication versionControlApplication = new VersionControlApplication();
    private PluginLoader pluginLoader = new PluginLoader();

    public static void main(String[] args) {
        versionControlApplication.start();
        versionControlApplication.stop();
    }

    private void start() {
        System.out.println("Starting Version Control Application");
        pluginLoader.loadAll();
    }

    private void stop() {
        System.out.println("Stopping Version Control Application");
        pluginLoader.unloadAll();
    }
}
